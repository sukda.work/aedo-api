//userRecordModel.js

var express = require("express");
var router = express.Router();
var userRecord = require("./userRecordModel");

// GET all
router.get("/", (req, res) => {
    userRecord.find().exec((err, data) => {
    if (err) return res.status(400).send(err);
    res.status(200).send(data);
  });
});

// GET by id
router.get("/:_id", (req, res) => {
    userRecord.findById(req.params._id).exec((err, data) => {
    if (err) return res.status(400).send(err);
    res.status(200).send(data);
  });
});



module.exports = router;